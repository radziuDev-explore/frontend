import GetTagsForUserQuery from '~/graphql/getTagsForUser.gql';
import SubscribeToTag from '~/graphql/subscribeToTag.gql';
import UnsubscribeFromTag from '~/graphql/unsubscribeFromTag.gql';
import ScrapTag from '~/graphql/scrapTag.gql';

interface Tag {
  id: number,
  userId: number,
  userTagId: number,
  name: string,
  services: string[],
  quantityOfPages: number,
  price: number,
  updatedAt: Date,
  createdAt: Date,
}

interface State {
  tags: Tag[]
};

export const useTagStore = defineStore({
  id: 'TagStore',
  state: (): State => {
    return {
      tags: [],
    };
  },

  getters: {
    getTags(state) {
      return state.tags;
    },
  },

  actions: {
    async fetchTags() {
      const { data, error } =  await useAsyncQuery(GetTagsForUserQuery, { errorPolicy: 'all' });
      if ((data.value as any)?.getTagsForUser) {
        this.tags = (data.value as any).getTagsForUser;
      } else {
        console.error(error);
      }
    },

    async subscribeToTag(variables: { tagName: string, quantityOfPages: number, services: string[] }) {
      const { mutate } =  useMutation(SubscribeToTag, { variables, errorPolicy: 'all' });
      const { data, errors }: any = await mutate();
      await throwGraphQLError(errors);

      return data;
    },

    async scrapTag(variables: { userTagId: number }) {
      const { mutate } =  useMutation(ScrapTag, { variables, errorPolicy: 'all' });
      const { data, errors }: any = await mutate();
      await throwGraphQLError(errors);

      return data;
    },

    async deleteTag(variables: { userTagId: number }) {
      const { mutate } =  useMutation(UnsubscribeFromTag, { variables, errorPolicy: 'all' });
      const { data, errors }: any = await mutate();
      await throwGraphQLError(errors);

      return data;
    }
  },
});