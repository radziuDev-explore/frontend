import RegisterQuery from '~/graphql/register.gql';
import CurrentQuery from '~/graphql/current.gql';

interface State {
  user: {
    id: number | null,
    name: string | null,
    email: string | null,
    balance: number | null,
    confirmedAt: Date | null,
    updatedAt: Date | null,
    createdAt: Date | null,
  },
};

export const useUserStore = defineStore({
  id: 'UserStore',
  state: (): State => {
    return {
      user: {
        id: null,
        name: null,
        email: null,
        balance: null,
        confirmedAt: null,
        updatedAt: null,
        createdAt: null,
      },
    };
  },

  getters: {
    getAll(state) {
      return state.user;
    },
  },

  actions: {
    async fetchCurrent() {
      const { data, error } =  await useAsyncQuery(CurrentQuery, { errorPolicy: 'all' });
      if ((data.value as any)?.current) {
        this.user = (data.value as any).current;
      } else {
        console.error(error);
      }
    },

    async createUser(variables: any) {
      const { mutate } =  useMutation(RegisterQuery, { variables, errorPolicy: 'all' });
      const { data, errors }: any = await mutate();
      await throwGraphQLError(errors);

      return data;
    }
  },
});