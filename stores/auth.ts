import LoginQuery from '~/graphql/login.gql';
import RefreshToken from '~/graphql/refreshToken.gql';
import RevokeRefreshToken from '~/graphql/revokeRefreshToken.gql';

export const useAuthStore = defineStore({
  id: 'AuthStore',

  actions: {
    async auth() {
      const accessToken = useCookie('accessToken')?.value || null;
      if (accessToken) return;
      
      const refreshToken = useCookie('refreshToken')?.value || null;
      if (!refreshToken) throw new Error('Unauthorized');
      
      await this.refresh();
    },

    async login(variables: Record<string, any>) {
      const { mutate } =  useMutation(LoginQuery, { variables, errorPolicy: 'all' });
      const { data, errors }: any = await mutate();
      await throwGraphQLError(errors);

      return data;
    },

    async refresh() {
      const accessTokenCookie = useCookie('accessToken').value;
      if (accessTokenCookie) {
        const payload = useAccessTokenPayload(accessTokenCookie as string);
        if (payload) {
          const issuedAt = payload.iat;
          const expiresAt = payload.exp;
          const now = Math.floor(Date.now() / 1000);
          if (expiresAt - now > 60 || now - issuedAt < 240) return;
        }
      }
      
      try {
        const { mutate } =  useMutation(RefreshToken);
        const { data, errors }: any = await mutate();
        await throwGraphQLError(errors);

        return data;
      } catch (error) {
        useCookie('accessToken').value = '';
        useCookie('refreshToken').value = '';

        throw error;
      }
    },

    async logout() {
      const { mutate } =  useMutation(RevokeRefreshToken);
      const { data, errors }: any = await mutate();
      await throwGraphQLError(errors);

      useCookie('accessToken').value = '';
      useCookie('refreshToken').value = '';
    },
  },
})