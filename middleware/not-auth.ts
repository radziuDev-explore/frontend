export default defineNuxtRouteMiddleware(async(to, from) => {
  try {
    const app = useNuxtApp();
    const auth = useAuthStore(app.$pinia);
    await auth.auth();
    return navigateTo('/');
  } catch {}
});