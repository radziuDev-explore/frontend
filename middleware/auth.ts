
export default defineNuxtRouteMiddleware(async (to, from) => {
  try {
    const app = useNuxtApp();
    const auth = useAuthStore(app.$pinia);
    await auth.auth();
    const user = useUserStore(app.$pinia);
    await user.fetchCurrent();
  } catch(error) {
    return navigateTo('/login');
  }
})