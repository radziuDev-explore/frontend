export const throwGraphQLError = async(errors: any) => {
  if (errors?.length) {
    if (Array.isArray(errors[0].message))
      throw Object.assign(new Error(errors[0].message[0]), { status: errors[0].statusCode || 400 });
    throw Object.assign(new Error(errors[0].message), { status: errors[0].statusCode || 400 });
  }
}