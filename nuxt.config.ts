export default defineNuxtConfig({
  typescript: {
    strict: true,
  },

  imports: {
    dirs: ['stores'],
  },

  modules: [
    '@nuxt/image-edge',
    'nuxt-icons',
    [
      '@nuxtjs/apollo',
      {
        autoImports: true,
        authType: 'Bearer',
        authHeader: 'Authorization',
        tokenStorage: 'cookie',
        cookieName: 'accessToken',
        proxyCookies: true,
        clients: {
          default: {
            httpLinkOptions: {
              credentials: 'include',
            },
            httpEndpoint: process.env.API_URL,
          },
        },
      },
    ],
    [
      '@pinia/nuxt',
      {
        autoImports: [
          'defineStore', 'acceptHMRUpdate',
          ['defineStore', 'definePiniaStore'],
        ],
      },
    ],
  ],

  css: [
    '@/assets/styles/main.scss',
    '@fortawesome/fontawesome-svg-core/styles.css',
  ],

  build: {
    transpile: [
      '@fortawesome/fontawesome-svg-core',
      '@fortawesome/free-solid-svg-icons',
      '@fortawesome/vue-fontawesome',
    ],
  },

  runtimeConfig: {
    public: {
      apiUrl: process.env.API_URL,
    },
  },

  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/styles/_variables.scss";',
        },
      },
    },
  },
})