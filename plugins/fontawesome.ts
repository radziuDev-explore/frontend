import { library, config } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faRefresh,
  faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faRefresh,
  faTrashAlt,
);

config.autoAddCss = false;

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('fa', FontAwesomeIcon);
});
