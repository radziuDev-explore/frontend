export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.hook("apollo:auth", async ({client, token}) => {
    const cookie = useCookie('accessToken');
    token.value = cookie.value || null;
  })
})