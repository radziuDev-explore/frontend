import 'tippy.js/dist/tippy.css';
import { plugin as VueTippy } from 'vue-tippy'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(VueTippy, {
    directive: 'tippy',
    interactive: true,
    theme: 'light',
    animateFill: false,
    arrow: true,
    arrowType: 'round',
    placement: 'bottom',
    trigger: 'click',
  });
});
